name := "greeter"

version := "0.1"

scalaVersion := "2.13.6"

javacOptions ++= Seq("-encoding", "UTF-8")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.9" % Test,
  "org.mockito" %% "mockito-scala-scalatest" % "1.16.42" % Test
)
