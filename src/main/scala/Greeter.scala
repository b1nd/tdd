import java.time.LocalTime

/** Write a Greeter class with greet function that
  * 1) receives a name as input and returns 'Hello <name>'
  * 2) greet capitalizes the first letter of the name
  * 3) greet trims the input
  * 4) greet throws error on blank input
  * 5) greet returns 'Good morning <name>' when the time is 06:00-12:00
  * 6) greet returns 'Good evening <name>' when the time is 18:00-22:00
  * 7) greet returns 'Good night <name>' when the time is 22:00-06:00
  * 8) greet logs into console each time it is called
  */
trait Greeter {
  def greet(name: String): String
}

object Validation {
  class NameIsBlankException extends Exception("name is blank")
}

trait TimeProvider {
  def getTime: LocalTime
}

trait Logger {
  def log(string: String): Unit
}

class LoggerImpl extends Logger {
  override def log(string: String): Unit = println(string)
}

class GreeterImpl(timeProvider: TimeProvider, logger: Logger) extends Greeter {
  override def greet(name: String): String = {
    val trimmedName = name.trim
    val capitalized = capitalize(trimmedName)
    val time        = timeProvider.getTime

    logger.log("Call greet")

    if (
      time.isAfter(LocalTime.of(6, 0)) &&
      time.isBefore(LocalTime.of(12, 0))
    ) {
      s"Good morning $capitalized"
    } else if (
      time.isAfter(LocalTime.of(18, 0)) &&
      time.isBefore(LocalTime.of(22, 0))
    ) {
      s"Good evening $capitalized"
    } else if (
      time.isAfter(LocalTime.of(22, 0)) ||
      time.isBefore(LocalTime.of(6, 0))
    ) {
      s"Good night $capitalized"
    } else {
      s"Hello $capitalized"
    }
  }

  private def capitalize(string: String): String = {
    if (string.isBlank) throw new Validation.NameIsBlankException
    string.substring(0, 1).toUpperCase + string.substring(1)
  }
}
