import Validation.NameIsBlankException
import org.mockito.scalatest.MockitoSugar
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.time.LocalTime

class GreeterTest
    extends AnyFlatSpec
    with Matchers
    with MockitoSugar
    with GivenWhenThen {

  trait mocks {
    val timeProvider = mock[TimeProvider]
    val logger       = mock[Logger]

    val greeter: Greeter = new GreeterImpl(timeProvider, logger)
  }

  behavior of "Greeter greet(name)"

  it should "simple greet" in new mocks {
    Given("Name Max")
    val name = "Max"

    When("time is 14:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(14, 0))
    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return Hello Max")
    val greeting = greeter.greet(name)

    greeting shouldBe "Hello Max"
  }

  it should "simple greet with correct name" in new mocks {
    Given("Name Alexey")
    val name = "Alexey"

    When("time is 14:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(14, 0))
    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return Hello Alexey")
    val greeting = greeter.greet(name)

    greeting shouldBe "Hello Alexey"
  }

  it should "capitalize first name letter" in new mocks {
    Given("Name with first lower letter")
    val name = "name"

    When("time is 14:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(14, 0))

    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return name with capitalized")
    val greeting = greeter.greet(name)

    greeting shouldBe "Hello Name"
  }

  it should "trim name" in new mocks {
    Given("Name with spaces around it")
    val name = "  name   "

    When("time is 14:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(14, 0))

    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return trimmed name")
    val greeting = greeter.greet(name)
    greeting shouldBe "Hello Name"
  }

  it should "throw error on blank name" in new mocks {
    Given("Empty name")
    val name = ""

    When("Greeter greets")
    Then("Throw an error")
    an[NameIsBlankException] shouldBe thrownBy(greeter.greet(name))
  }

  it should "return Good morning when time is 06:00-12:00" in new mocks {
    Given("Name Max")
    val name = "Max"

    When("time is 10:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(10, 0))
    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return morning greetings name")
    val greeting = greeter.greet(name)

    greeting shouldBe "Good morning Max"
  }

  it should "return Good evening when time is 18:00-22:00" in new mocks {
    Given("Name Max")
    val name = "Max"

    When("time is 19:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(19, 0))
    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return evening greetings name")
    val greeting = greeter.greet(name)

    greeting shouldBe "Good evening Max"
  }

  it should "return Good evening when time is 22:00-06:00" in new mocks {
    Given("Name Max")
    val name = "Max"

    When("time is 4:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(4, 0))
    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return night greetings name")
    val greeting = greeter.greet(name)

    greeting shouldBe "Good night Max"
  }

  it should "log greetings call" in new mocks {
    Given("Name Max")
    val name = "Max"

    When("time is 14:00")
    when(timeProvider.getTime).thenReturn(LocalTime.of(14, 0))
    When("logger logs")
    when(logger.log(any[String])).thenAnswer(())

    Then("Return day greetings")
    val greeting = greeter.greet(name)
    Then("Greeter logged call")
    verify(logger).log("Call greet")

    greeting shouldBe "Hello Max"
  }
}
